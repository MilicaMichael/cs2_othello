#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) 
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     this->side = side;
     if(side == WHITE)
     {
		this->other_side = BLACK;
	 }
	 else
	 {
		 this->other_side = WHITE;
	 }
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{   
	board.doMove(opponentsMove, other_side);
	
	if(depth < 2)
	{			
		vector<Move *> moves = board.getMoves(side);
		vector<Player *> children(moves.size());

		for(unsigned int i = 0; i < moves.size(); i++)
		{			
			children[i] = new Player(other_side);
			children[i]->board = *board.copy();
			children[i]->depth = this->depth + 1;
			children[i]->doMove(moves[i], msLeft);
		}
		
		if(depth % 2 == 1)
		{			
			int min_score = 64;
			for(unsigned int i = 0; i < children.size(); i++)
			{
				if(children[i]->score < min_score)
				{
					min_score = children[i]->score;
				}
			}	
			
			this->score = min_score;
			return NULL;
		}		
		
		else
		{
			if(children.size() == 0)
			{
				return NULL;
			}
			
			int max_index = -1;
			int max_score = -64;
			for(unsigned int i = 0; i < children.size(); i++)
			{
				if(children[i]->score > max_score)
				{
					max_index = i;
					max_score = children[i]->score;
				}	
			}
			this->score = max_score;
			board.doMove(moves[max_index], side);			
			return moves[max_index];
		}
	}
	
	else
	{		
		score = board.getScore(side, other_side);
		return NULL;
	}
}	
	/*
	Move * move = NULL;
	score = -64;
	list<Move *> moves = board.getMoves(side);
	
	int size = moves.size();
	for(int i = 0; i < size; i++)
	{		 
		Move * m = moves.front();
		moves.pop_front();
	
		Board * board2 = board.copy();
		board2->doMove(m, side);
	
		int d;
		d = board2->count(side) - board2->count(other_side); 
		
		if(m->x == 0 || m->x == 7)
		{
			d += 4;
		}
		if(m->y == 0 || m->y == 7)
		{
			d += 4;
		}
		
		if(d > score)
		{
			move = m;
			score = d;				 
		}
			
		delete board2;	
	}
	return move; 	     
} */
